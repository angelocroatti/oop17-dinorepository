package application;

import javafx.application.Application;

/**
 * This class start the game.
 * 
 * @author POGGI GIOVANNI
 */
public class GameInit {

  /**
   * This method start the game.
   * @param args of method
   */
  public static void main(String args[]) {
    Application.launch(view.sceneLoader.SceneLoader.class, args);
  }
}