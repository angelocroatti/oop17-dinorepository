package game.environment;

import java.awt.Graphics;

/**
 * Abstract class to admit to add different objects in the future in the sky.
 * 
 * @author POGGI GIOVANNI
 */
public abstract class Ambientation {
    
  /**
     * This method refresh and update all elements of the game in the "Sky".
     * @param speed of the game.
  */
  public abstract void update(double speed);
  
  /**
     * This method draw the environment of the game.
     * @param env is the environment of all element of the game.
  */
  public abstract void drawEnvironment(Graphics env);
}