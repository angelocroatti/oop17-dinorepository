package game.mainCharacter;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;


import util.Animation;

/**
 * The Trex class, extending MainCharacter class, manages the main character's operations.
 * 
 * 
 * @author FRANCESCO VITALI
 *
 */
public class Trex extends MainCharacter {

  /**
   * The main character's position in the land.
   */
  public static final int LAND_POSY = 80;
  
  /**
   * This constant determines the speed of the jump.
   */
  public static final double GRAVITY = 0.4;

  private double posY;
  private double posX;
  private double fallingSpeed;
  private Rectangle rectBound;
  private MainCharacterState state;

  private Animation normalRunAnim;
  private Animation downRunAnim;
  private BufferedImage jumpImage;
  private BufferedImage deathImage;

  /**
   * The Constructor initialize a Object from this class giving him a posx and a posy,
   * a state of gaming and a bound to manage the collision between the Trex and the obstacles.
   */
  public Trex() {
    posX = 50;
    posY = LAND_POSY;
    this.state = MainCharacterState.NORMAL_RUN;
    rectBound = new Rectangle();
        
    try {
      normalRunAnim = new Animation(90);
      normalRunAnim.addFrame(ImageIO.read(this.getClass()
            .getResourceAsStream("/game/mainCharacter/trex_running1.png")));
      normalRunAnim.addFrame(ImageIO.read(this.getClass()
            .getResourceAsStream("/game/mainCharacter/trex_running2.png")));
      jumpImage = ImageIO.read(this.getClass()
            .getResourceAsStream("/game/mainCharacter/trex_waitANDjump.png"));
      
      downRunAnim = new Animation(90);
      downRunAnim.addFrame(ImageIO.read(this.getClass()
            .getResourceAsStream("/game/mainCharacter/trex_duck1.png")));
      downRunAnim.addFrame(ImageIO.read(this.getClass()
            .getResourceAsStream("/game/mainCharacter/trex_duck2.png")));
      deathImage = ImageIO.read(this.getClass()
            .getResourceAsStream("/game/mainCharacter/trex_dead.png"));
    } catch (IOException e1) {
      System.out.println("Trex .png Error: " + e1.getMessage());
    }
  }

  @Override
  public void draw(Graphics g) {
    switch (state) {    
      case NORMAL_RUN:
        g.drawImage(normalRunAnim.getFrame(), (int) posX, (int) posY, null);
        break;
      case JUMPING:
        g.drawImage(jumpImage, (int) posX, (int) posY, null);
        break;
      case DOWN_RUN:
        g.drawImage(downRunAnim.getFrame(), (int) posX, (int) (posY + 20), null);
        break;
      case DEAD:
        g.drawImage(deathImage, (int) posX, (int) posY, null);
        break;
      default:
        break;
    }
  }

  @Override
  public void update() {
    normalRunAnim.updateFrame();
    downRunAnim.updateFrame();
    if (posY >= LAND_POSY) {
      posY = LAND_POSY;
      if (state != MainCharacterState.DOWN_RUN) {
        state = MainCharacterState.NORMAL_RUN;
      }
    } else {
      fallingSpeed += GRAVITY;
      posY += fallingSpeed;
    }
  }

  @Override
  public void jump() {
    if (posY >= LAND_POSY) {
      
      fallingSpeed = -7.5;
      posY += fallingSpeed;
      state = MainCharacterState.JUMPING;
    }
  }

  @Override
  public void down(boolean isDown) {
    if (state == MainCharacterState.JUMPING) {
      return;
    }
    if (isDown) {
      state = MainCharacterState.DOWN_RUN;
    } else {
      state = MainCharacterState.NORMAL_RUN;
    }
  }

  @Override
  public Rectangle getBound() {
    if (state == MainCharacterState.DOWN_RUN) {
      rectBound.x = (int) posX + 5;
      rectBound.y = (int) posY + 20;
      rectBound.width = downRunAnim.getFrame().getWidth() - 10;
      rectBound.height = downRunAnim.getFrame().getHeight();
    } else {
      rectBound.x = (int) posX + 5;
      rectBound.y = (int) posY;
      rectBound.width = normalRunAnim.getFrame().getWidth() - 10;
      rectBound.height = normalRunAnim.getFrame().getHeight();
    }
    return rectBound;
  }

  @Override
  public void dead(boolean isDeath) {
    if (isDeath) {
      
      state = MainCharacterState.DEAD;
    } else {
      state = MainCharacterState.NORMAL_RUN;
    }
  }

  @Override
  public void reset() {
    posY = LAND_POSY;
  }
}