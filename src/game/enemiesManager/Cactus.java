package game.enemiesManager;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;

/**
 * This class manage the obstacles named "Cactus" and refresh
 * the element in the game every time something call the method
 * update. It also do the creation and the delete of the cactus in the game.
 * 
 * @author POGGI GIOVANNI
 */
public class Cactus extends Enemy {

  /**
   * This field save the vertical position of the cactus in the game.
   */
  private static final int POSY_IN_THE_LAND = 125;
  /**
   * This field save the x position of the obstacle.
   */
  private int positionX;
  /**
   * This field save the width of the cactus.
   */
  private int width;
  /**
   * This field save the height of the cactus.
   */
  private int height;
  /**
   * This field save one of the two different image of cactus.
   */
  private BufferedImage image;
  /**
   * This field save the rectangle to put the cactus' image.
   */
  private Rectangle rectangle;
  
  /**
   * Prepare the fields to use.
   * @param positionX set the x position of cactus.
   * @param width set the width of cactus.
   * @param height set the height of cactus.
   * @param image set one of the two images of the cactus.
   */
  public Cactus(final int positionX, final int width, final int height, final BufferedImage image) {
    this.positionX = positionX;
    this.width = width;
    this.height = height;
    this.image = image;
    rectangle = new Rectangle();
  }

  @Override
  public Rectangle getBound() {
    rectangle = new Rectangle();
    rectangle.x = (int) positionX + (image.getWidth() - width) / 2;
    rectangle.y = POSY_IN_THE_LAND - image.getHeight() + (image.getHeight() - height) / 2;
    rectangle.width = width;
    rectangle.height = height;
    return rectangle;
  }

  @Override
  public boolean isOutOfScreen() {
    if (positionX < -image.getWidth()) {
      return true;
    }
    return false;
  }

  @Override
  public void update(final double speed) {
    positionX -= speed;
  }

  @Override
  public void drawEnvironment(final Graphics env) {
    env.drawImage(image, positionX, POSY_IN_THE_LAND - image.getHeight(), null);
  }
}