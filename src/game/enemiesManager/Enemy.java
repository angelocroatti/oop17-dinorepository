package game.enemiesManager;

import java.awt.Graphics;
import java.awt.Rectangle;

/**
 * This class is abstract because, if we want to add some
 * new different enemies, we can use this class to implement
 * some general methods.
 * 
 * @author POGGI GIOVANNI
 */
public abstract class Enemy {

  /**
   * This class refresh the game and update all elements.
   * @param speed of the game.
   */
  public abstract void update(double speed);
  
  /**
   * This method draw the environment of the game.
   * @param env is the environment of all element of the game.
   */
  public abstract void drawEnvironment(Graphics env);

  /**
   * This method create a rectangle and set the dimension of cactus' image.
   * @return the rectangle just created.
   */
  public abstract Rectangle getBound();

  /**
   * This method control if the elements are out of the screen
   * or not and, eventually, delete them.
   * @return true if the element is out of screen, false otherwise.
   */
  public abstract boolean isOutOfScreen();
}