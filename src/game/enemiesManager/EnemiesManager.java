package game.enemiesManager;

import game.mainCharacter.MainCharacter;
import game.speed.SpeedManager;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.imageio.ImageIO;


/**
 * This class manage the enemy of the game, it create, delete and draw an enemy.
 * 
 * @author POGGI GIOVANNI
 */
public class EnemiesManager {

  /**
   * This field save first different type of cactus' image.
   */
  private BufferedImage cactus1;
  /**
   * This field save second different type of cactus' image.
   */
  private BufferedImage cactus2;
  /**
   * This field save a random number.
   */
  private Random rand;
  /**
   * This field save a list with all enemies.
   */
  private List<Enemy> enemies;
  /**
   * This field save the main character.
   */
  private MainCharacter mainCharacter;
  /**
   * This field save the speed of the game.
   */
  private SpeedManager speed;
  
  /**
   * This constructor prepare a random number, upload two different
   * type of cactus' image, create an array list with all enemies,
   * save the main character and create the first enemy.
   * @param mainCharacter of the game.
   * @param speed of the game.
   */
  public EnemiesManager(final MainCharacter mainCharacter, final SpeedManager speed) {
    rand = new Random();
    this.speed = speed;
    try {
      cactus1 = ImageIO.read(this.getClass()
            .getResourceAsStream("/game/enemiesManager/cactus1.png"));
      cactus2 = ImageIO.read(this.getClass()
            .getResourceAsStream("/game/enemiesManager/cactus2.png"));
    } catch (IOException e) {
      System.out.println("Enemy .png Error: " + e.getMessage());
    }    
    enemies = new ArrayList<Enemy>();
    this.mainCharacter = mainCharacter;
    enemies.add(createEnemy());
  }

  /**
   * This method create an enemy choosing from two different type of they.
   * @return the enemy that it will create.
   */
  private Enemy createEnemy() {
    int type = rand.nextInt(2);
    if (type == 0) {
      return new Cactus(800, cactus1.getWidth() - 10, cactus1.getHeight() - 10, cactus1);
    } else {
      return new Cactus(800, cactus2.getWidth() - 10, cactus2.getHeight() - 10, cactus2);
    }
  }

  /**
   * This class manage the collision between main character and the enemies.
   * @return true if there's a collision, false otherwise.
   */
  public boolean isCollision() {
    for (Enemy e : enemies) {
      if (mainCharacter.getBound().intersects(e.getBound())) {
        return true;
      }
    }
    return false;
  }

  /**
   * This class reset the game and prepare to create new enemies.
   */
  public void reset() {
    enemies.clear();
    enemies.add(createEnemy());
  }

  /**
   * This class refresh the game and update all objects.
   */
  public void update() {
    for (Enemy e : enemies) {
      e.update(this.speed.getSpeed());
    }
    Enemy enemy = enemies.get(0);
    if (enemy.isOutOfScreen()) {
      enemies.clear();
      enemies.add(createEnemy());
    }
  }

  /**
   * This method draw the environment of the game.
   * @param env is the environment with all element of the game.
   */
  public void drawEnvironment(final Graphics env) {
    for (Enemy e : enemies) {
      e.drawEnvironment(env);
    }
  }
}