package game.speed;

/**
 * This Class manages the game mode with the speed that increases progressively.
 * @author ROMANELLI AURORA
 *
 */
public class ProgressiveSpeed implements SpeedManager  {

  private static final double MAX_SPEED =  8;
  private static final double INC =  0.003;
  private static final double START_SPEED =  2;  
  private double speed;
  private final Boolean mode;
  private double acceleration;
  private float base;
 
  /**
   * Constructor.
   */
  
  public ProgressiveSpeed() {
    this.speed = START_SPEED;
    this.acceleration = 0;
    this.base = 0;
    this.mode = false;    
  }
  
  @Override  
  public void updateSpeed() {
    if (mode == false) {
      if (speed < MAX_SPEED) {
        speed = START_SPEED + incAcceleration();
      } else  if (speed >= MAX_SPEED) {
        speed = MAX_SPEED;
      }
    }
  }   
  
  @Override
  public void reset() {
    this.speed = START_SPEED;
    this.acceleration = 0;
    this.base = 0;
  }

  @Override
  public double getSpeed() {
    return speed;
  }

  private double incAcceleration() {
    increaseX();
    acceleration = ((Math.log(base + 1)) / (Math.log(2)));
    return  acceleration;
  }

  private double increaseX() {
    base += INC; 
    return base;
  }
}
