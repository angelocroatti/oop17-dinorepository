package game.score;

/**
 * Interface for the score manager,
 * reusable for different implementation in the calculation of the score. 
 * @author ROMANELLI AURORA
 *
 */
public interface ScoreManager {

  /**
  * Getter.
 * @return the actual score of the game.
 */
  public Integer getScore();
  
  /**
 * Method that increment the score based on the actual speed.
 */
  public void incrementScore();
  
  
  /**
  * Method that reset the parameters related to the score.
  */
  public void reset();
  
 


}
