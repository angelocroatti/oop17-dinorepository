package test;

import game.enemiesManager.EnemiesManager;
import game.environment.Land;
import game.environment.Sky;
import game.mainCharacter.MainCharacter;
import game.mainCharacter.Trex;
import game.speed.ProgressiveSpeed;
import game.speed.SpeedManager;
import org.junit.Test;
import view.windows.GameWindow;

import static org.junit.Assert.assertTrue;

/**
 * This Class do some test for the main objects of the game.
 * 
 * @author POGGI GIOVANNI
 */
public class CreateObjectTest {

  /**
   * This method check if the field cactus exist or not.
   * @param cactus the enemies of the game.
   * @return true if the field cactus exist, false otherwise.
   */
  public boolean createCactus(final EnemiesManager cactus) {
    return cactus != null;
  }

  /**
   * This method check if the field land exist or not.
   * @param land the land of the game.
   * @return true if the field land exist, false otherwise.
   */
  public boolean createLand(final Land land) {
    return land != null;
  }

  /**
   * This method check if the field cloud exist or not.
   * @param cloud the cloud of the game in the sky.
   * @return true if the field cloud exist, false otherwise.
   */
  public boolean createCloud(final Sky cloud) {
    return cloud != null;
  }

  /**
   * This method check if the field trex exist or not.
   * @param trex mainCharacter of the game.
   * @return true if the field trex exist, false otherwise.
   */
  private boolean createMainCharacter(final MainCharacter trex) {
    return trex != null;
  }

  /**
   * Test creation of all object of the game.
   */
  @Test
  public void testCreationObject() {
    final SpeedManager speed = new ProgressiveSpeed();
    final MainCharacter mainCharacter = new Trex();
    final EnemiesManager cactus = new EnemiesManager(mainCharacter, speed);
    final Sky cloud = new Sky(GameWindow.SCREEN_WIDTH);
    final Land land = new Land(GameWindow.SCREEN_WIDTH);
    assertTrue(createCactus(cactus));
    assertTrue(createMainCharacter(mainCharacter));
    assertTrue(createLand(land));
    assertTrue(createCloud(cloud));
  }
}