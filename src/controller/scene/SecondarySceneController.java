package controller.scene;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import game.score.RankingManager;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.TextArea;
import javafx.scene.layout.Pane;
import view.sceneLoader.FXMLPath;

/**
 * This class manage the secondary scene (High Score and How To Play) of the game.
 * 
 * @author POGGI GIOVANNI
 */
public class SecondarySceneController implements Initializable {

  @FXML
  private Pane rootPane;
  @FXML
  private TextArea tablescore; 

  private RankingManager manager = new RankingManager();
  private String score = "";

  /**
   * This method open the scene Game when user press the button Back.
   * @param event of the button.
   * @throws IOException is the method fails to load scene.
   */
  @FXML
  private void pressBack(ActionEvent event) throws IOException {
    this.rootPane.getChildren().clear();
    Parent root = FXMLLoader.load(getClass().getResource(FXMLPath.GAME.getPath()));
    this.rootPane.getChildren().add(root);
  }

  @Override
  public void initialize(final URL arg0, final ResourceBundle arg1) {
    if (this.rootPane.getChildren().stream()
                                       .filter(l -> l.equals(this.tablescore))
                                       .findFirst().isPresent()) {
      score = manager.getHighscoreString();
      tablescore.setText(score);
    }
  }
}