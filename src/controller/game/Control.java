package controller.game;

import game.enemiesManager.EnemiesManager;
import game.environment.Land;
import game.environment.Sky;
import game.mainCharacter.MainCharacter;
import game.mainCharacter.Trex;
import game.score.MyScoreManager;
import game.speed.FixedSpeed;
import game.speed.ProgressiveSpeed;
import game.speed.Run;
import game.speed.SpeedManager;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JPanel;

import util.Difficulty;
import view.windows.GameOverWindow;
import view.windows.GameWindow;

/**
 * The Control class is the main controller of the game, connecting the view and the model.
 * 
 * @author FRANCESCO VITALI, AURORA ROMANELLI
 */
public class Control extends JPanel implements Controller, KeyListener {

  private static final long serialVersionUID = 1L;
  
  private static final int START_GAME_STATE = 0;
  private static final int GAME_PLAYING_STATE = 1;
  private static final int GAME_OVER_STATE = 2;
  private final Land land;
  private final MainCharacter mainCharacter;
  private final EnemiesManager enemiesManager;
  private final SpeedManager speed;
  private final MyScoreManager score;
  private final Sky clouds;
  private boolean isKeyPressed;
  private int gameState;
  private final Run run;

  /**
   * Constructor for the classic mode.
   */
  public Control() {
    run = new Run(this);
    gameState = START_GAME_STATE;
    mainCharacter = new Trex();
    land = new Land(GameWindow.SCREEN_WIDTH);
    speed = new ProgressiveSpeed();
    score = new MyScoreManager(speed);
    enemiesManager = new EnemiesManager(mainCharacter, speed);
    clouds = new Sky(GameWindow.SCREEN_WIDTH);
    
  }

  /**
   * Constructor for the fixed difficulty mode.
    * @param diff difficulty of the game.
    */
  public Control(final Difficulty diff) {
    run = new Run(this);
    gameState = START_GAME_STATE;
    mainCharacter = new Trex();
    land = new Land(GameWindow.SCREEN_WIDTH);
    speed = new FixedSpeed(diff);
    score = new MyScoreManager(speed);
    enemiesManager = new EnemiesManager(mainCharacter, speed);
    clouds = new Sky(GameWindow.SCREEN_WIDTH);
  }

  @Override
  public void gameStart() {
    run.startGame();
  }

  @Override
  public void gameOver() {
    run.stopGame(); 
  }

  @Override
  public void gameUpdate() {
    if (gameState == GAME_PLAYING_STATE) {
      clouds.update(speed.getSpeed());
      land.update(speed.getSpeed());
      mainCharacter.update();
      enemiesManager.update();
      speed.updateSpeed();
      if (enemiesManager.isCollision()) {
        gameState = GAME_OVER_STATE;
        mainCharacter.dead(true);
      }
    }
  }

  @Override
  public void keyPressed(final KeyEvent e) {
    if (!isKeyPressed) {
      isKeyPressed = true;
      
      switch (gameState) {
        case START_GAME_STATE:
          if (e.getKeyCode() == KeyEvent.VK_SPACE) {
            mainCharacter.jump();
            gameState = GAME_PLAYING_STATE;
          }
          break;
        case GAME_PLAYING_STATE:
          if (e.getKeyCode() == KeyEvent.VK_SPACE) {
            mainCharacter.jump();
          } else if (e.getKeyCode() == KeyEvent.VK_DOWN) {
            mainCharacter.down(true);
          }
          break;
        default:
          break;
      }
    }
  }
  
  @Override
  public void keyReleased(final KeyEvent e) {
    isKeyPressed = false;
    if (gameState == GAME_PLAYING_STATE && e.getKeyCode() == KeyEvent.VK_DOWN) {
      mainCharacter.down(false);
    }
  }
  
  @Override
  public void keyTyped(final KeyEvent e) {}

  @Override
  public void paint(final Graphics g) {
    g.setColor(Color.decode("#f7f7f7"));
    g.fillRect(0, 0, getWidth(), getHeight());
               
    switch (gameState) {
      case START_GAME_STATE:
        clouds.drawEnvironment(g);
        land.drawEnvironment(g);
        enemiesManager.drawEnvironment(g);
        mainCharacter.draw(g);
        break;
        
      case GAME_PLAYING_STATE:
        drawAll(g);
        g.setColor(Color.BLACK);
        
        g.drawString("HI: " + score.getRanking().getScoreAtPosition(0)
            + "  " + score.getScore(), 500, 20); 
        break;
        
      case GAME_OVER_STATE:
        drawAll(g);
        g.setColor(Color.BLUE);
        g.drawString("HI: " + score.getRanking().getScoreAtPosition(0)
            + "  " + score.getScore(), 500, 20);

        gameOver();
        new GameOverWindow(this);
        break;
        
      default:
        break;

    }
  }

  private void drawAll(final Graphics g) {
    clouds.drawEnvironment(g);
    land.drawEnvironment(g);
    enemiesManager.drawEnvironment(g);
    mainCharacter.draw(g);
    score.incrementScore();
  }

  @Override
  public void gameReset() {
    gameState = GAME_PLAYING_STATE;
    enemiesManager.reset();
    mainCharacter.dead(false);
    mainCharacter.reset();
    speed.reset();
    score.reset();
  }

  /**
   * Method that insert the player in the ranking.
   * @param name of the player.
   */
  public void addToRanking(final String name) {
    score.addScore(name);
    
  }
  
  /**
   * Getter.
 * @return the score manager.
 */
  public MyScoreManager getScoreManager() {
    return score;
  }

}