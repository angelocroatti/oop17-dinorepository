package view.windows;

import controller.game.Control;
import javax.swing.JFrame;
import util.Difficulty;

/**

 * This class creates, manages and runs the window of the running game, with two constructors:
 * one starts the game with a speed that grows exponentially, the other with a speed relative to
 * the value of the parameter diff.
 * 
 * @author FRANCESCO VITALI
 */
public class GameWindow extends JFrame {

  /**
   * Serial version UID of the class GameWindow.
   */
  private static final long serialVersionUID = 1L;
  
  /**
   * The following fields, in order of declaration, are used to
   * save the width of the screen and the
   * field control from class Control.
   */
  public static final int SCREEN_WIDTH = 575;
  private Control control;

  /**
   * Create and manage the window of the game.
   */
  public GameWindow() {
    super("Dino Runner Game");
    setSize(SCREEN_WIDTH, 185);
    setLocation(400, 200);
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    setResizable(false);
  }

  /**
   * This method starts the game by calling the class Control.
   */
  public void startGame() {
    this.control = new Control();
    addKeyListener(this.control);
    add(this.control);
    setVisible(true);
    this.control.gameStart();
  }

  /**
   * This method starts the game in fixed mode.
   * @param diff the difficulty of the game.
   */
  public void startFixedGame(Difficulty diff) {
    this.control = new Control(diff);
    addKeyListener(this.control);
    add(this.control);
    setVisible(true);
    this.control.gameStart();
  }
}
