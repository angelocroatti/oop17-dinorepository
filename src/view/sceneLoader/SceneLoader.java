package view.sceneLoader;

import java.io.IOException;
import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * This class start and manage the main scene of the game.
 * 
 * @author POGGI GIOVANNI
 */
public class SceneLoader extends Application {

  /**
   * This field is the main stage of the game.
   */
  private Stage primaryStage = new Stage();

  @Override
  public void start(final Stage primaryStage) throws IOException {
    this.primaryStage.setTitle("Dino Runner");
    this.primaryStage.initStyle(StageStyle.TRANSPARENT);
    showGame();
  }
  
  /**
   * This method show the men� of the game.
   * @throws IOException if the method fails to save the field.
   */
  @FXML
  private void showGame() throws IOException {
    Parent root;
    FXMLLoader loader = new FXMLLoader();
    loader.setLocation((getClass().getResource(FXMLPath.GAME.getPath())));
    root = loader.load();
    primaryStage.setScene(new Scene(root));
    primaryStage.show();
  }
}